import QtQuick 2.4
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3

Item {
    width: 400
    height: 400
    property alias submitProposalButton: submitProposalButton
    property alias backButton: backButton

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent

        Label {
            id: label1
            text: qsTr("Propose a New Project")
            Layout.fillHeight: true
            Layout.fillWidth: true

            Text {
                id: text1
                x: 30
                y: 41
                text: qsTr("Project Title")
                font.pixelSize: 15
            }
        }

        RowLayout {
            id: rowLayout1
            width: 100
            height: 100

            Button {
                id: submitProposalButton
                text: qsTr("Submit")
            }

            Button {
                id: backButton
                text: qsTr("Back")
            }
        }
    }
}
