import QtQuick 2.4

LoginPageForm {
    id: loginPage
    width: parent.width
    height: parent.height
    loginButton.onClicked: stack.push(Qt.resolvedUrl("ProjectMap.qml"))
}
