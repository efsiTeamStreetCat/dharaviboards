import QtQuick 2.4
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 1200
    height: 1920
    property alias loginButton: loginButton
    property alias nameFieldLabel: nameFieldLabel
    property alias nameField: nameField

    Image {
        id: image1
        anchors.fill: parent
        z: -1
        source: "dharaviBoards_login_image.png"
    }

    TextField {
        id: nameField
        y: 1406
        height: 43
        anchors.right: parent.right
        anchors.rightMargin: 150
        anchors.left: parent.left
        anchors.leftMargin: 150
        anchors.bottom: loginButton.top
        anchors.bottomMargin: 34
        Layout.fillHeight: false
        Layout.maximumHeight: 5535
        Layout.preferredHeight: 80
        Layout.preferredWidth: 640
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        Layout.fillWidth: true
        placeholderText: qsTr("Enter your name here")
    }

    Button {
        id: loginButton
        y: 440
        width: 100
        height: 100
        text: qsTr("Login")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 300
        anchors.left: parent.left
        anchors.leftMargin: 70
        anchors.right: parent.right
        anchors.rightMargin: 70
        scale: 0.9
        Layout.rowSpan: 1
        Layout.fillHeight: false
        Layout.maximumHeight: 5535
        Layout.preferredHeight: 100
        Layout.preferredWidth: 640
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        checkable: false
    }

    Label {
        id: nameFieldLabel
        x: 288
        height: 100
        color: "#ffffff"
        text: "Facilitating Community Driven Projects"
        anchors.top: parent.top
        anchors.topMargin: 268
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 20
        Layout.maximumHeight: 5535
        Layout.preferredHeight: 30
        Layout.preferredWidth: 640
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
    }



}
