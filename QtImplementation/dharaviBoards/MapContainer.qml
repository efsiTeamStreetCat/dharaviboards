import QtQuick 2.4
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3
import QtPositioning 5.5
import QtLocation 5.6

Rectangle {
    width: 200
    height: 200
    color: "#ffffff"
    Layout.fillHeight: true
    Layout.minimumHeight: 500
    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
    Layout.fillWidth: true

    //! [Initialize Plugin]
    Plugin {
        id: myPlugin
        name: "osm"
        //specify plugin parameters if necessary
        //PluginParameter {...}
        //PluginParameter {...}
        //...
    }
    //! [Initialize Plugin]

    //! [Current Location]
    PositionSource {
        id: positionSource
        property variant lastSearchPosition: locationOslo
        active: true
        updateInterval: 120000 // 2 mins
        onPositionChanged:  {
            var currentPosition = positionSource.position.coordinate
            map.center = currentPosition
            var distance = currentPosition.distanceTo(lastSearchPosition)
            if (distance > 500) {
                // 500m from last performed pizza search
                lastSearchPosition = currentPosition
                searchModel.searchArea = QtPositioning.circle(currentPosition)
                searchModel.update()
            }
        }
    }
    //! [Current Location]



    //! [Places MapItemView]
    Map {
        id: map
        anchors.fill: parent
        plugin: myPlugin;
        center: locationOslo
        zoomLevel: 13

        MapQuickItem {
                coordinate: QtPositioning.coordinate( 24.523038, 54.435267 )

                anchorPoint.x: image.width * 0.5
                anchorPoint.y: image.height

                sourceItem: Column {
                    Image { id: image; source: "marker.png" }
                    Text { text: "Jumping castle on the highline!"; font.bold: true }
                }
            }

        MapQuickItem {
                coordinate: QtPositioning.coordinate( 24.515542, 54.424138 )

                anchorPoint.x: image2.width * 0.5
                anchorPoint.y: image2.height

                sourceItem: Column {
                    Image { id: image2; source: "marker.png" }
                    Text { text: "Install a new boat ramp"; font.bold: true }
                }
            }


    }
    //! [Places MapItemView]

    Connections {
        target: searchModel
        onStatusChanged: {
            if (searchModel.status == PlaceSearchModel.Error)
                console.log(searchModel.errorString());
        }
    }
}
