import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5

Button {
    id: Button
    text: qsTr("Back")
    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
    onClicked: if (stack) {
                   stack.push(Qt.resolvedUrl("ProjectMapForm.ui.qml"))
               }
}
