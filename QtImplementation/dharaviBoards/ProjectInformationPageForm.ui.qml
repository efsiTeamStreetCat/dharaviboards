import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5

Item {
    id: item1
    width: 480
    height: 640

    GridLayout {
        id: gridLayout1
        anchors.fill: parent
        columnSpacing: 0
        rowSpacing: 0
        rows: 5
        columns: 1

        Label {
            id: projectTitle
            text: qsTr("Build a community organization app for Dharavi")
            Layout.maximumHeight: 80
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            font.bold: true
            font.pointSize: 24
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
        }


        Image {
            id: mainProjectImage
            width: 100
            height: 100
            Layout.rowSpan: 1
            Layout.maximumHeight: 250
            fillMode: Image.PreserveAspectCrop
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            Layout.fillHeight: false
            Layout.fillWidth: true
            source: "shien.jpg"
        }




        Text {
            id: projectDescription
            text: qsTr("A recent trip to a ‘slum’ in India shed light on the lack of agency many locals face in their communities. In many areas, designated leaders meet and discuss issues and opportunities however the rest of the community tends to be left without a voice for their ideas and values. Our project aims to provide all community members with the opportunity to have input and lead significant change. \n")
            anchors.top: item1.bottom
            anchors.topMargin: -139
            verticalAlignment: Text.AlignTop
            wrapMode: Text.WordWrap
            Layout.rowSpan: 1
            Layout.maximumHeight: 100
            Layout.fillWidth: true
            Layout.fillHeight: false
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
            font.pixelSize: 12
        }

        Button {
            id: addPhotoButton
            text: qsTr("Add progress photo")
            iconSource: qsTr("")
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        Rectangle {
            id: chatPlaceholder
            width: 200
            height: 200
            color: "#c3a40a"
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}
