import QtQuick 2.0


Rectangle {
    width: 600; height: 960

    Component {
        id: contactDelegate
        Item {
            width: parent.width; height: 80
            Column {
                Text { text: '<b>Name:</b> ' + name }
                Text { text: '<b>Number:</b> ' + number }
            }
        }
    }

    ListView {

        spacing: 15
        anchors.fill: parent
        model: ContactModel {}
        delegate: contactDelegate


    }
}
