import QtQuick 2.4

ProjectMapForm {
    id: projectMap
    width: parent.width
    height: parent.height
    addNewProjectButton.onClicked: stack.push(Qt.resolvedUrl("AddNewProject.qml"))
    listViewButton.onClicked: stack.push(Qt.resolvedUrl("ProjectList.qml"))
}
