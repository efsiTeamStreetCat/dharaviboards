import QtQuick 2.4
import QtQuick.Controls 1.5
import QtQuick.Layouts 1.3

Item {
    width: 480
    height: 640
    property alias mapContainer: mapContainer
    property alias listViewButton: listViewButton
    property alias addNewProjectButton: addNewProjectButton

    ColumnLayout {
        id: columnLayout1
        anchors.fill: parent

        MapContainer {
            id: mapContainer
        }

        RowLayout {
            id: rowLayout1
            width: 100
            height: 100

            Button {
                id: addNewProjectButton
                text: qsTr("Add New Project")
                Layout.fillHeight: true
                Layout.fillWidth: true
            }

            Button {
                id: listViewButton
                text: qsTr("List View")
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }
}
