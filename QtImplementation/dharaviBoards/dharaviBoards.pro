TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc


# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    LoginPageForm.ui.qml \
    LoginPage.qml \
    ProjectListForm.ui.qml \
    ProjectList.qml \
    ProjectMapForm.ui.qml \
    ProjectMap.qml\
    AddNewProject.qml\
    AddNewProjectForm.ui.qml\
    ProjectInformationPage.qml\
    ProjectInformationPageForm.ui.qml




