import QtQuick.Dialogs 1.2
import QtQuick 2.2
import QtQuick.Controls 1.5
import QtQml.Models 2.2

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Dharavi Boards")

    StackView {
        id: stack
        anchors.fill: parent
        initialItem: Qt.resolvedUrl("LoginPage.qml")
    }

//    ProjectList {
//        id: listview
//        anchors.fill: parent
//        homeButton.onClicked: stack.pop()
//    }

//    menuBar: MenuBar {
//        Menu {
//            title: qsTr("Navigation")
//            MenuItem {
//                text: qsTr("Map View")
//                onTriggered: if (stack) stack.pop(Qt.resolvedUrl("ProjectMapForm.ui.qml"))
//            }
//            MenuItem {
//                text: qsTr("List View")
//                onTriggered: if (stack) stack.find(function(item, index) {return item.id=="projectMap"})
//            }
//        }
//    }

}
