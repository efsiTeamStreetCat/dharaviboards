#Working Title: Dharavi Boards

##Overview
This is a mobile application to facilitate bottom-up organization of community improvement projects developed by a group of undergraduate engineering students at the NYU Abu Dhabi [Engineering Design Studio](http://nyuad.io), as part of the Spring 2016 instalment of the *Engineers for Social Impact* course.

The founding members of the team are Daniel Carelli, Jovan Jovancevic, Shien Yang Lee, and William Young. They refer to themselves as *Team Steetcat* , in identification with a cat whose existence was, at that point in time, under heavy scrutiny from various parties on the NYU Abu Dhabi campus as the university grew and innovation and risk taking came up against bureaucracy and liability.

The inspiration for this project came from a field lab experience in Dharavi, Mumbai. Through interactions with local residents and activists working in the area, the team saw many examples of successful bottom-up community initiatives surrounding issues ranging from negotiating housing redeveloping schemes with governmental agencies to providing communal WiFi access. During their time in Dharavi, the team also encountered an strong sense of solidarity within communities and a desire for agency and empowerment amongst residents, many of whom belong to traditionally oppressed groups.

Dharavi is a space that has been touted as "Asia's biggest slum" and received heavy attention from NGOs and other aid organizations. The team believes that *empowering* people to organize and improve their lives and communities from the bottom up is a more financially sustainable and more dignified approach that supports people without perpetuating hegemonic binaries of power and privilege that are often associated with traditional top-down *aid* models.

##Features
* Propose community improvement projects
* Visualize proposed projects by location
* Subscribe to projects of interest to receive updates
* Engage in public discussion surrounding proposed projects
* Organize in-person meetings to implement project
* Track project progress

##Architecture
Dharavi Boards is developed on the Qt framework for Android devices with a combination of a QML UI and C++ application logic.
