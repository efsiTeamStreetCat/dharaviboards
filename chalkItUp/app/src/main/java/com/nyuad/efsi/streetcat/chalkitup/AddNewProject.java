package com.nyuad.efsi.streetcat.chalkitup;


import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.text.TextUtils;

import com.firebase.client.Firebase;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class AddNewProject extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public String fbTitle;
    private Firebase mRef;
    private EditText eteditTitle, eteditDescription, eteditCost, eteditDate, eteditPhone;
    private GoogleApiClient mGoogleApiClient;
    public static final String TAG = AddNewProject.class.getSimpleName();
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private LocationRequest mLocationRequest;
    String Latitude = "";
    String Longitude = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_project);
        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://appchalkitup.firebaseio.com");
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);
        eteditTitle = (EditText) findViewById(R.id.editTitle);
        //checkTitle = eteditTitle.getText().toString();

        eteditDescription = (EditText) findViewById(R.id.editDescription);
        eteditCost = (EditText) findViewById(R.id.editCost);
        eteditDate = (EditText) findViewById(R.id.editDate);
        eteditPhone = (EditText) findViewById(R.id.editPhone);
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(AppIndex.API).build();
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AddNewProject Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.nyuad.efsi.streetcat.chalkitup/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "AddNewProject Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.nyuad.efsi.streetcat.chalkitup/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    private void attemptSubmit(String title) {
        View focusView = null;
        eteditTitle.setError(null);

        if (TextUtils.isEmpty(title)) {
            eteditTitle.setError(getString(R.string.error_field_required));
            focusView = eteditTitle;
            focusView.requestFocus();
        }


    }



    public class project{
        private String Title, Description, Cost, Date, Phone, User, Messages;
        public project(){}

        public project(String Title, String Description, String Cost, String Date, String Phone, String User, String Latitude, String Longitude, String Messages){
            this.Title=Title;
            this.Description=Description;
            this.Cost=Cost;
            this.Date=Date;
            this.Phone=Phone;
            this.User = User;
            this.Messages = Messages;
            //this.Latitude = Latitude;
           // this.Longitude = Longitude;
        }
        public String getTitle() {
            return Title;
        }

        public String getDescription() {
            return Description;
        }

        public String getCost() {
            return Cost;
        }

        public String getDate() {
            return Date;
        }

        public String getPhone() {
            return Phone;
        }

        public String getUser() {
            return User;
        }

        public String getLatitude() {
            return Latitude;
        }

        public String getLongitude() {
            return Longitude;
        }

        public String getMessages() {
            return Messages;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "Location services connected.");
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        };

    }

    private void handleNewLocation(Location location) {
        Log.d(TAG, location.toString());
        Latitude = String.valueOf(location.getLatitude());
        Longitude = String.valueOf(location.getLongitude());

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Location services suspended. Please reconnect.");
    }


    public void onButtonClick(View v) {
        if (v.getId() == R.id.B_cancel) {
            Intent i = new Intent(AddNewProject.this, MarkerMap.class);
            startActivity(i);
        } else if (v.getId() == R.id.B_submitProject) {

            String date = SimpleDateFormat.getDateInstance().format(new Date());
            String Title = eteditTitle.getText().toString() +" - "+ date;
            attemptSubmit(Title);
            String Description = eteditDescription.getText().toString();
            String Cost = eteditCost.getText().toString();
            String Date = eteditDate.getText().toString();
            String Phone = eteditPhone.getText().toString();
            String User = LoginActivity.mFullnameView.getText().toString();
            String username = LoginActivity.musernameView.getText().toString();
            String Messages = " ";

            //fbTitle = Title +"-"+ time;
            Firebase proposal = mRef.child("Projects").child(Title);
            project project_current = new project(Title,Description,Cost,Date,Phone, User, Latitude, Longitude, Messages);
            proposal.setValue(project_current);

            String time = SimpleDateFormat.getDateTimeInstance().format(new Date());
            Firebase active = mRef.child("Users/"+username).child("Proposals");
            Map<String, Object> active_map = new HashMap<String, Object>();
            active_map.put(time, Title);
            active.updateChildren(active_map);

            Intent i = new Intent(AddNewProject.this, MarkerMap.class);
            startActivity(i);
        }

        setContentView(R.layout.add_new_project);

    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
}