package com.nyuad.efsi.streetcat.chalkitup;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ProjectListView extends AppCompatActivity {

    int projectNumber;
    String[] projectName = new String[100];
    String[] projectDescription = new String[100];
    private Firebase mRef;
    private Firebase Ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int pr = 0;
        setContentView(R.layout.activity_project_list_view);

        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://appchalkitup.firebaseio.com/Projects");
        Ref = new Firebase("https://appchalkitup.firebaseio.com");
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                projectNumber = (int) snapshot.getChildrenCount();
                System.out.println(projectNumber);


                int temp_pr = pr;

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {

                    try {

                        projectName[temp_pr] = postSnapshot.child("title").getValue().toString();
                        projectDescription[temp_pr] = postSnapshot.child("description").getValue().toString();
                        System.out.println("Hello");
                        temp_pr++;

                    } catch (Exception e) {
                        System.out.println("IN CATCH");
                    }
                }
                populateUsersList();

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {


            }
        });


    }


    public String name;
    public String hometown;

    public void User(String name, String hometown) {
        this.name = name;
        this.hometown = hometown;

    }

    public ArrayList<User> getUsers() {
        ArrayList<User> users = new ArrayList<User>();
        System.out.println("ProjectNumber: " + projectNumber);
        for (int i = 0; i < projectNumber; i++) {
            //System.out.println("i is" + i);
            //System.out.println(projectName[i]);
            users.add(new User(projectName[i],projectDescription[i]));
        }
        return users;
    }


    private void populateUsersList() {
        // Construct the data source
        ArrayList<User> arrayOfUsers = getUsers();
        System.out.println(arrayOfUsers);
        // Create the adapter to convert the array to views
        CustomUsersAdapter adapter = new CustomUsersAdapter(this, arrayOfUsers);
        // Attach the adapter to a ListView
        ListView listView = (ListView) findViewById(R.id.lvUsers);
        listView.setAdapter(adapter);listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                System.out.println("LOOK HERE!");
                System.out.println(projectName[(int) (parent.getAdapter().getItemId(position))]);

//                System.out.println("Anything?");
                String user = LoginActivity.musernameView.getText().toString();
                Firebase active = Ref.child("Users/"+user).child("Active Project");
                active.setValue(projectName[(int) (parent.getAdapter().getItemId(position))]);
                System.out.println(projectName[(int) (parent.getAdapter().getItemId(position))]);
                Intent i = new Intent(ProjectListView.this, SpecificProjectDescription.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_project_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.b_map_view) {
            Intent i = new Intent(ProjectListView.this, MarkerMap.class);
            startActivity(i);
        } else if (id == R.id.b_new_project) {
            Intent i = new Intent(ProjectListView.this, AddNewProject.class);
            startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }
}
