package com.nyuad.efsi.streetcat.chalkitup;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import org.w3c.dom.Text;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SpecificProjectChat extends AppCompatActivity {

    private Firebase mRef;
    private Firebase messages;
    private Firebase rRef;
    String project;
    String allMessages = "";

    private EditText eteditInputMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        scrollDown();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_project_chat);
        Firebase.setAndroidContext(this);

        mRef = new Firebase("https://appchalkitup.firebaseio.com");
        rRef = new Firebase("https://appchalkitup.firebaseio.com");
        String user_test = LoginActivity.musernameView.getText().toString();
        Firebase active_project = mRef.child("Users/"+user_test).child("Active Project");

        active_project.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                project = snapshot.getValue().toString();
                System.out.println("PROJECT" + " " + project);
                try{
                    messages = new Firebase("https://appchalkitup.firebaseio.com/Projects").child(project).child("messages");
                    feedMessages();
                }
                catch (Exception e){
                    System.out.println("Hopefully created messages directory");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

                System.out.println("The read failed: " + firebaseError.getMessage());
            }
        });

    }

    public class message {
        private String textMessage;

        public message() {
        }

        public message(String textMessage) {
            //this.uniqueID = uniqueID;
            this.textMessage = textMessage;
        }

        public String getUniqueId() {
            Long tsLong = System.currentTimeMillis() / 1000;
            String ts = tsLong.toString();
            String time = SimpleDateFormat.getDateTimeInstance().format(new Date());
            System.out.println(time);
            String username = LoginActivity.musernameView.getText().toString();
            String userWithTime = ts;
            userWithTime += " - ";
            userWithTime += time;
            userWithTime += " - ";
            userWithTime += username;
            return userWithTime;
        }

        public String getTextMessage() {
            return textMessage;
        }
    }

    //public String project;

    public void onButtonClick(View v) {

        if (v.getId() == R.id.buttonSend) {
            System.out.println("In1");
            eteditInputMessage = (EditText) findViewById(R.id.editInputMessage);
            String contentsMessage = eteditInputMessage.getText().toString();
            EditText et = (EditText) findViewById(R.id.editInputMessage);
            et.setText("");
            feedMessages();

            message message_current = new message(contentsMessage);
            //System.out.println("Unique ID is: " + message_current.getUniqueId());

            //Change contents of .child(HERE) to make it reference the title of the opened project
            String user_test = LoginActivity.musernameView.getText().toString();
            Firebase active_project = mRef.child("Users/"+user_test).child("Active Project");


            active_project.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    project = snapshot.getValue().toString();
                    //System.out.println("PROJECT" + " " + project);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                    System.out.println("The read failed: " + firebaseError.getMessage());
                }
            });
            //First message does not go through because it does not have a directory to write to.
            try{
                Map<String, Object> message_map = new HashMap<String, Object>();
                message_map.put(message_current.getUniqueId(), contentsMessage);
                messages.updateChildren(message_map);
            }
            catch (Exception e){
               System.out.println("In2");
            }
            scrollDown();


        }

    }


    //messages.push().setValue(message_map);
    //messages.setValue(message_current);
    // message_current = new uniqueID(contentsMessage);
    //newMessage.setValue(message_current);

    // mRef.child("Proposal/Messages").setValue(R.id.editInputMessage);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.b_specific_project_description) {
            Intent i = new Intent(SpecificProjectChat.this, SpecificProjectDescription.class);
            startActivity(i);
        } else if (id == R.id.b_list_view) {
            Intent i = new Intent(SpecificProjectChat.this, ProjectListView.class);
            startActivity(i);
        } else if (id == R.id.b_map_view) {
            Intent i = new Intent(SpecificProjectChat.this, MarkerMap.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_specific_project_chat, menu);
        return true;
    }

    void scrollDown()

    {
        Thread scrollThread = new Thread(){
            public void run(){
                try {
                    sleep(200);
                    SpecificProjectChat.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ((ScrollView) findViewById(R.id.scroll1)).fullScroll(View.FOCUS_DOWN);

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        scrollThread.start();

    }
    public void feedMessages(){
        try{
            Firebase read_project = rRef.child("Projects").child(project).child("messages");

            read_project.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {

                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        String attribute = postSnapshot.getKey().toString();
                        String attributeValue = postSnapshot.getValue().toString();
                        allMessages += attribute;
                        allMessages += ": ";
                        allMessages += attributeValue;
                        allMessages += '\n';
                        TextView DisplayTitle = (TextView) findViewById(R.id.messageView);
                        DisplayTitle.setText(allMessages);

                    //String attribute = snapshot.getValue().toString();
                    //String attributeValue = snapshot.getValue().toString();

                    }
                    allMessages = "";
                }



                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

        }
        catch (Exception e){}
    }
}

