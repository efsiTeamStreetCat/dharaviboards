package com.nyuad.efsi.streetcat.chalkitup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;


public class SpecificProjectDescription extends AppCompatActivity {
    private Firebase mRef;
    private Firebase rRef;
    String project;
    String projectName;
    String projectDescription;
    String projectCost;
    String projectPhone;
    String owner;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_project_description);
        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://appchalkitup.firebaseio.com");
        rRef = new Firebase("https://appchalkitup.firebaseio.com");

        String user_test = LoginActivity.musernameView.getText().toString();
        Firebase active_project = mRef.child("Users/"+user_test).child("Active Project");
        active_project.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                project = snapshot.getValue().toString();
                System.out.println("PROJECT IS" + project);
                updateProject();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.b_specific_project_chat) {
            Intent i = new Intent(SpecificProjectDescription.this, SpecificProjectChat.class);
            startActivity(i);
        } else if (id == R.id.b_list_view) {
            Intent i = new Intent(SpecificProjectDescription.this, ProjectListView.class);
            startActivity(i);
        } else if (id == R.id.b_map_view) {
            Intent i = new Intent(SpecificProjectDescription.this, MarkerMap.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_project_map_view_investigate, menu);
        return true;
    }

    public void updateProject(){
        System.out.println("PROJECT2 IS" + project);
        try{
            Firebase read_project = rRef.child("Projects").child(project);

            read_project.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    System.out.println("SNAPSHOT:" + snapshot.getValue().toString());

                    String [][] projectDetails = new String [(int)snapshot.getChildrenCount()][2];
                    int cnt = 0;
                    String Cost = null, Title = " ", Description = " ", User = null, Date = null, Phone = null, Feed = " ";
                    for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                        String attribute = postSnapshot.getKey().toString();
                        projectDetails[cnt][0] = attribute;
                        String attributeValue = postSnapshot.getValue().toString();
                        projectDetails[cnt][1] = attributeValue;
                        if (attribute == "cost") {
                            Cost = attributeValue;
                        }else if (attribute == "description") {
                            Description = attributeValue;
                        }else if (attribute == "date") {
                            Date = attributeValue;
                        }else if (attribute == "phone") {
                            Phone = attributeValue;
                        }else if (attribute == "user") {
                            User = attributeValue;
                        }else{
                            Title = attributeValue;
                        }
                        cnt++;
                        }

                    Feed = "PROJECT: \n";
                    Feed += Title;
                    Feed += "\n \nDESCRIPTION: \n";
                    Feed += Description;
                    if (Cost != null){
                        Feed += "\n \nCOST: \n";
                        Feed += Cost;
                    }
                    if (Date != null){
                        Feed += "\n \nNEXT MEETING DATE: \n";
                        Feed += Date;
                    }
                    if (Phone != null){
                        Feed += "\n \nCONTACT PROPOSER: \n";
                        Feed += User;
                        Feed += ": ";
                        Feed += Phone;
                    }
                    TextView DisplayTitle = (TextView) findViewById(R.id.feedView);
                    DisplayTitle.setText(Feed);


                  /*  TextView DisplayTitle = (TextView) findViewById(R.id.titleView);
                    DisplayTitle.setText(Title);
                    TextView DisplayDescription = (TextView) findViewById(R.id.descriptionView);
                    DisplayDescription.setText(Description);
                    TextView DisplayCost = (TextView) findViewById(R.id.costView);
                    DisplayCost.setText(Cost);
                    TextView DisplayPhone = (TextView) findViewById(R.id.phoneView);
                    DisplayPhone.setText(Phone);
                    TextView DisplayDate = (TextView) findViewById(R.id.dateView);
                    DisplayDate.setText(Date);*/
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

        }
        catch (Exception e){}
    }
}
