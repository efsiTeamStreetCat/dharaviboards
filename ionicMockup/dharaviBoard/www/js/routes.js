angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('mapView', {
    url: '/map_view',
    templateUrl: 'templates/mapView.html',
    controller: 'mapViewCtrl'
  })

  .state('listView', {
    url: '/list_view',
    templateUrl: 'templates/listView.html',
    controller: 'listViewCtrl'
  })

  .state('project1LandingPage', {
    url: '/project_1',
    templateUrl: 'templates/project1LandingPage.html',
    controller: 'project1LandingPageCtrl'
  })

  .state('addNewProject', {
    url: '/add_new_project',
    templateUrl: 'templates/addNewProject.html',
    controller: 'addNewProjectCtrl'
  })

$urlRouterProvider.otherwise('/map_view')

  

});